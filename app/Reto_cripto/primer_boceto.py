from tkinter import filedialog
from tkinter import *
import os

from Crypto.PublicKey import DSA                        
from Crypto.Signature import DSS                    
from Crypto.Hash import SHA256  

import wget

root = Tk()

path = os.getcwd()

root.title("CryptApp")
#root.iconbitmap(path + r'\app\Reto_cripto\crypto.ico')
root.geometry("400x400")

def file_read():
    global message
    file_name = filedialog.askopenfilename(initialdir= "*",
                                               title= "Select File",
                                               filetypes=(("pdf files","*.pdf"), ("All Files","*.*")))
    
    with open(file_name, 'rb') as f:
        message = f.read()

    if message == None:
        l5 = Label(root, text= "Archivo vacío.")
        l5.place(x=200, y=130, anchor="center")
    else:
        l6 = Label(root, text= "Archivo leído con éxito.")
        l6.place(x=200, y=130, anchor="center")
    bp1["state"] = DISABLED

def get_key():
    global key_file
    url = 'https://raw.githubusercontent.com/rafneta/MA2006B/main/archivos/public_key.pem'
    key_file = wget.download(url)

    bp2["state"] = DISABLED

def get_signature():
    global signature
    file_key = filedialog.askopenfilename(initialdir= "*",
                                               title= "Select File",
                                               filetypes=(("pdf files","*.pdf"), ("All Files","*.*")))
    
    with open(key_file, 'rb') as f:
        signature = f.read()

    if signature == None:
        l5 = Label(root, text= "Archivo vacío.")
        l5.place(x=200, y=250, anchor="center")
    else:
        l6 = Label(root, text= "Archivo leído con éxito.")
        l6.place(x=200, y=250, anchor="center")
    bp3["state"] = DISABLED

def verify():
  with open('public_key.pem', 'rb') as f:
    hash_obj = SHA256.new(message) 
    pub_key = DSA.import_key(f.read()) 
    verifier = DSS.new(pub_key, 'fips-186-3')
  try:
    verifier.verify(hash_obj, signature)
    l7 = Label(root, text= "El mensaje es auténtico")
    l7.place(x=200, y=310, anchor="center")       
  except ValueError:
    l8 = Label(root, text= "El mensaje no es auténtico")
    l8.place(x=200, y=310, anchor="center")
    
welcomeLabel = Label(root, text= "Bienvenido a la aplicación", font=("Arial Black", 14))
welcomeLabel.place(x=200, y=25, anchor="center")

lp1 = Label(root, text= "Seleccione el archivo a encriptar")
lp1.place(x=150, y=100, anchor="center")
bp1 = Button(root, text= "Leer Archivo", command= file_read)
bp1.place(x=300, y=100, anchor="center")

lp2 = Label(root, text= "Llave pública")
lp2.place(x=150, y=160, anchor="center")
bp2 = Button(root, text= "Obtener llave", command= get_key)
bp2.place(x=300, y=160, anchor="center")

lp3 = Label(root, text= "Lea el archivo de la firma")
lp3.place(x=150, y=220, anchor="center")
bp3 = Button(root, text= "Firma", command= get_signature)
bp3.place(x=300, y=220, anchor="center")


bp4 = Button(root, text= "Verificar autenticidad", command= verify)
bp4.place(x=200, y=280, anchor="center")


mainloop()