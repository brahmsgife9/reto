import mysql.connector

from tkinter import *
from tkinter import messagebox

import csv


root = Tk()

db = mysql.connector.connect(
    
    host="localhost",
    user="root",
    passwd="root",
    database="testdatabase"
)

#Creación de base de datos.
mycursor = db.cursor()
#mycursor.execute("CREATE TABLE Usuario (id int PRIMARY KEY AUTO_INCREMENT, nombre VARCHAR(50), apellidoP VARCHAR(50), apellidoM VARCHAR(50), email VARCHAR(50), dpto VARCHAR(50), cargo VARCHAR(50))")

####FUNCIONES

def cerrar():
    root.destroy()

def insert_data():
    db = mysql.connector.connect(
    
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    cursor = db.cursor()
    sql = "INSERT INTO Usuario (id, nombre, apellidoP, apellidoM, email, dpto, cargo)\
       VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}')".format(id1.get(),nom.get(),apep.get(),apem.get(),email.get(),dpto.get(),cargo.get())
    
    try:
        cursor.execute(sql)
        db.commit()
        id1.delete(0, 'end')
        nom.delete(0, 'end')
        apep.delete(0, 'end')
        apem.delete(0, 'end')
        email.delete(0, 'end')
        dpto.delete(0, 'end')
        cargo.delete(0, 'end')
        messagebox.showinfo(message="Registro exitoso", title="Aviso")
    except:
        db.rollback()
        messagebox.showerror(message="No se pudo ejecutar el registro",title="Error fatal")
        
        db.close()

def delete():
    
    db = mysql.connector.connect(
    
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    cursor = db.cursor()
    sql = "DELETE FROM Usuario WHERE id='"+id1.get()+"'"
    
    try:
        cursor.execute(sql)
        db.commit()
        id1.delete(0, 'end')
        nom.delete(0, 'end')
        apep.delete(0, 'end')
        apem.delete(0, 'end')
        email.delete(0, 'end')
        dpto.delete(0, 'end')
        cargo.delete(0, 'end')
        messagebox.showinfo(message="Borrado exitoso", title="Aviso")
    except:
        db.rollback()
        messagebox.showerror(message="No se pudo eliminar el registro",title="Error fatal")
        
        db.close()

def update():

    db = mysql.connector.connect(
    
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    cursor = db.cursor()
    
    sql = "UPDATE Usuario SET nombre='"+nom.get()+"', apellidoP='"+apep.get()+"', apellidoM='"+apem.get()+"',email='"+email.get()+"', dpto='"+dpto.get()+"', cargo='"+cargo.get()+"' WHERE id='"+id1.get()+"'"

    try:
        cursor.execute(sql)
        db.commit()
        id1.delete(0, 'end')
        nom.delete(0, 'end')
        apep.delete(0, 'end')
        apem.delete(0, 'end')
        email.delete(0, 'end')
        dpto.delete(0, 'end')
        cargo.delete(0, 'end')
        messagebox.showinfo(message="Actualización exitosa", title="Aviso")
    except:
        db.rollback()
        messagebox.showerror(message="No se pudo actualizar el registro",title="Error fatal")
        
        db.close() 

def consult():
    if id1.get() == "":
        messagebox.showinfo("Obteniendo consulta")
    else:
        db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
        cursor = db.cursor()
        cursor.execute("SELECT * FROM Usuario WHERE id='"+id1.get()+"'")
        id1.delete(0, 'end')
        nom.delete(0, 'end')
        apep.delete(0, 'end')
        apem.delete(0, 'end')
        email.delete(0, 'end')
        dpto.delete(0, 'end')
        cargo.delete(0, 'end')
        
        rows = cursor.fetchall()

        for row in rows:
            nom.insert(0, row[1])
            apep.insert(0, row[2])
            apem.insert(0, row[3])
            email.insert(0, row[4])
            dpto.insert(0, row[5])
            cargo.insert(0, row[6])
        
        db.close()

def gen_csv():
    
    db = mysql.connector.connect(
    
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    cursor = db.cursor()
    cursor.execute("SELECT * FROM Usuario")
    rows = cursor.fetchall()
    
    csv_fields = ["ID", "NOMBRE", "APELLIDO PATERNO",
              "APELLIDO MATERNO", "EMAIL", "DEPARTAMENTO",
              "CARGO"]
    
    
    csv_rows=[]
    
    for row in rows:
        row = list(row)
        csv_rows.append(row)
    
    with open('datos.csv', 'w') as f:
        write = csv.writer(f)
        
        write.writerow(csv_fields)
        write.writerows(csv_rows)
    
    db.close()
    
root.title("Prueba SQL")
root.geometry("350x800")
root.configure(background="#C5C5C5")

image = PhotoImage(file=r"C:\Users\carav\OneDrive\Escritorio\python\Tkinter_course\optima.png")
image = image.subsample(4,4)
label = Label(image=image)
label.pack()

##REGISTRO

#ID
e = Label(root, text="ID", bg="gray", fg="white")
e.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
id1 = Entry(root)
id1.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)

#Nombre
e = Label(root, text="Nombre", bg="gray", fg="white")
e.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
nom = Entry(root)
nom.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)

#Apellido Paterno
e1 = Label(root, text="Apellido Paterno", bg="gray", fg="white")
e1.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
apep = Entry(root)
apep.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)

#Apellido Materno
e2 = Label(root, text="Apellido Materno", bg="gray", fg="white")
e2.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
apem = Entry(root)
apem.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)

#Email
e3 = Label(root, text="Email", bg="gray", fg="white")
e3.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
email = Entry(root)
email.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)

#Departamento
e4 = Label(root, text="Departamento", bg="gray", fg="white")
e4.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
dpto = Entry(root)
dpto.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)

#Cargo
e5 = Label(root, text="Cargo", bg="gray", fg="white")
e5.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
cargo = Entry(root)
cargo.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)

##BOTONES

#Registrar
b = Button(root, text="Registrar", fg="black", command=insert_data)
b.pack(side=LEFT)

#Consultar
b1 = Button(root, text="Consultar", fg="black", command=consult)
b1.pack(side=LEFT)

#Actualizar
b2 = Button(root, text="Actualizar", fg="black", command= update)
b2.pack(side=LEFT)

#Eliminar
b3 = Button(root, text="Eliminar", fg="black", command=delete)
b3.pack(side=LEFT)

#Salir
b4 = Button(root, text="Salir", fg="black", command=cerrar)
b4.pack(side=LEFT)

#Show
b5 = Button(root, text="Generar CSV", fg="black", command=gen_csv)
b5.pack(side=LEFT)

mainloop()