import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
from tkinter import filedialog

def open_file():
    global archivo
    archivo = filedialog.askopenfilename(title="Seleccione archivo")
    return archivo
 
LARGEFONT =("Verdana", 35)
  
class tkinterApp(tk.Tk):
     
    # __init__ function for class tkinterApp
    def __init__(self, *args, **kwargs):
         
        # __init__ function for class Tk
        tk.Tk.__init__(self, *args, **kwargs)
         
        # creating a container
        container = tk.Frame(self) 
        container.pack(side = "top", fill = "both", expand = True)
  
        container.grid_rowconfigure(0, weight = 1)
        container.grid_columnconfigure(0, weight = 1)
  
        # initializing frames to an empty array
        self.frames = {} 
  
        # iterating through a tuple consisting
        # of the different page layouts
        for F in (login, admin, user, verify, firmar, registrar):
  
            frame = F(container, self)
  
            # initializing frame of that object from
            # startpage, page1, page2 respectively with
            # for loop
            self.frames[F] = frame
  
            frame.grid(row = 0, column = 0, sticky ="nsew")
  
        self.show_frame(login)
  
    # to display the current frame passed as
    # parameter
    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()
    
    def open_file():
        global archivo
        archivo=filedialog.askopenfilename(title="Seleccione archivo")
        return archivo
# first window frame startpage
  
class login(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label_user = ttk.Label(self, text ="user", font = LARGEFONT)
        label_user.grid(row = 5, column = 4, padx = 50, pady = 10)
        user_entry = ttk.Entry(self)
        user_entry.grid(row = 6, column = 4, padx = 50, pady = 10)
        
        label_pswd = ttk.Label(self, text ="password", font = LARGEFONT)
        label_pswd.grid(row = 8, column = 4, padx = 50, pady = 10)
        pswd_entry = ttk.Entry(self)
        pswd_entry.grid(row = 9, column = 4, padx = 50, pady = 10)
        
        login_button = ttk.Button(self,text = "Log in", command = lambda:controller.show_frame(admin))
        login_button.grid(row = 10, column = 4, padx = 50, pady = 10)

  
    def get_data():
        global usuario
        global contra
        usuario = user_entry.get()
        contra = pswd_entry.get()

  
  
# second window frame page1
class admin(tk.Frame):
     
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        registro_button = ttk.Button(self,text="Registro de \n nuevo usuario",command = lambda:controller.show_frame(registrar))
        registro_button.grid(row = 5, column = 4, padx = 50, pady = 10)
        
        
        act_cert_button = ttk.Button(self,text="Actualizar\n certificado",command = lambda:controller.show_frame(firmar))
        act_cert_button.grid(row = 6, column = 4, padx = 50, pady = 10)
        
        firmar_button = ttk.Button(self,text = "Firmar\n documento",command = lambda:controller.show_frame(firmar))
        firmar_button.grid(row = 7, column = 4, padx = 50, pady = 10)
        
        verificar_button = ttk.Button(self,text = "Verificar\n firma",command = lambda:controller.show_frame(verify))
        verificar_button.grid(row = 8, column = 4, padx = 50, pady = 10)

        


  
  
  
# third window frame page2
class user(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        
        firmar_button = tk.Button(self,text = "Firmar\n documento",height = 7, width = 25)
        firmar_button.grid(row = 5, column = 4, padx = 50, pady = 10)
        
        verificar_button = tk.Button(self,text = "Verificar\n firma",height = 7, width = 25)
        verificar_button.grid(row = 6, column = 4, padx = 50, pady = 10)
        
class verify(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        upload_button = tk.Button(self,text = "Subir archivo.", command = lambda:open_file())
        upload_button.grid(row = 5, column = 4, padx = 50, pady = 10)
        
        signerkey_label = tk.Label(self,text = "Firmador")
        signerkey_label.grid(row = 5, column = 4, padx = 50, pady = 10)
        signerkey_entry = tk.Entry(self)
        signerkey_entry.grid(row = 6, column = 4, padx = 50, pady = 10)
        
        verify_button = tk.Button(self,text = "Verificar")
        verify_button.grid(row = 7, column = 4, padx = 50, pady = 10)
        
        back_button = tk.Button(self, text = "<-", command = lambda:controller.show_frame(admin))
        back_button.grid(row = 0, column = 0, padx = 0, pady = 10)
    

class firmar(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        upload_button = ttk.Button(self,text = "Subir archivo.", command = lambda:open_file())
        upload_button.grid(row = 5, column = 4, padx = 50, pady = 10)
        verify_button = ttk.Button(self,text = "Firmar")
        verify_button.grid(row = 8, column = 4, padx = 50, pady = 10)
        back_button = tk.Button(self, text = "<-", command = lambda:controller.show_frame(admin))
        back_button.grid(row = 3, column = 0, padx = 50, pady = 10)
    
   
    
        
        
class registrar(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        #Linea de nombre
        usern_label = ttk.Label(self,text = "Usuario:")
        usern_label.grid(row = 4, column = 4, padx = 50, pady = 10)
        usern_entry = tk.Entry(self)
        usern_entry.grid(row = 5, column = 4, padx = 50, pady = 10)
        #Linea de contraseña
        cont_label = tk.Label(self,text = "Contraseña:")
        cont_label.grid(row = 7, column = 4, padx = 50, pady = 10)
        cont_entry = tk.Entry(self,show="*")
        cont_entry.grid(row = 8, column = 4, padx = 50, pady = 10)
        #Asignacion y generacion de pub key
        
        #Asigancion ID  
        ID_label = tk.Label(self,text = "ID:")
        ID_label.grid(row = 10, column = 4, padx = 50, pady = 10)
        ID_entry = tk.Entry(self)
        ID_entry.grid(row =11, column = 4, padx = 50, pady = 10)
        
        #Boton de confirmacion
        
        login_button = tk.Button(self,text = "Registrar")
        login_button.grid(row = 8, column = 4, padx = 50, pady = 10)
        
        back_button = tk.Button(self, text = "<-", command = lambda:controller.show_frame(admin))
        back_button.grid(row = 0, column = 0, padx = 50, pady = 10)

  
  
# Driver Code
app = tkinterApp()
app.mainloop()