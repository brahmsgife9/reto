from tkinter import *
from tkinter import ttk, END, HORIZONTAL, Frame, Toplevel
import time
import conexion


class Login(Frame):
    
    def __init__(self, master, *args):
        super().__init__(master, *args)
        
        self.user_marcar = "Ingrese su usuario"
        self.contra_marcar = "Ingresa su contraseña"
        
        self.fila1 = ''
        self.fila2 = ''
        self.datos = conexion.Registro_datos()
        self.widgets()
        
    def entry_out(self, event, event_text):
        if event['fg'] == 'black' and len(event.get()) == 0:
            event.delete(0, END)
            event['fg'] = 'grey'
            event.insert(0, event_text)
            
        if self.entry2.get() != 'Ingrese su contraseña':
            self.entry2['show'] = ""
        
        if self.entry2.get() != 'Ingrese su usuario':
            self.entry2['show'] = "*"
        
    def entry_in(self, event):
        if event['fg'] == 'grey':
            event['fg'] = 'black'
            event.delete(0, END)
        
        if self.entry2.get() != 'Ingrese su contraseña':
            self.entry2['show'] = "*"
            
        if self.entry2.get() != 'Ingrese su contraseña':
            self.entry2['show'] = ""
    
    def salir(self):
        self.master.destroy()
        self.master.quit()
        
    def acceder_ventana_dos(self):
        for i in range(101):
            self.barra['value'] += 1
            self.master.update()
            time.sleep(0.02)
    
        self.master.withdraw()
        self.ventana_dos = Toplevel()
        self.ventana_dos.title('Funciones')
        self.ventana_dos.geometry("500x500+400+80")
        self.ventana_dos.protocol("WM_DELETE_WINDOW", self.salir)
        self.ventana_dos.config(bg = "white")
        self.ventana_dos.state('zoomed')
    
        Label(self.ventana_dos, text= "VENTANA DOS", font= "Arial 40", bg= "white").pack(expand=True)
        Button(self.ventana_dos, text= "Salir", font= "Arial 10", bg= "red", command= self.salir).pack(expand=True)

    def verifica_user(self):
        self.indica1['text'] = ''
        self.indica2['text'] = ''
        users_entry = self.entry.get()
        password_entry = self.entry.get()
        
        dato1 = self.datos.busca_user(users_entry)
        dato2 = self.datos.busca_pass(password_entry)
        
        self.fila1 = dato1
        self.fila2 = dato2
        
        if self.fila1 == self.fila2:
            if dato1 == [] and dato2 == []:
                self.indica2['text'] = "Contraseña incorrecta"
                self.indica1['text'] = "Usuario incorrecto"
            else:
                if dato1 == []:
                    self.indica1['text'] = "Usuario incorrecto"
                else:
                    dato1 = dato1[0][1]
                if dato2 == []:
                    self.indica2['text'] = "Contraseña incorrecta"
                else:
                    dato2 = dato2[0][2]
                
                if dato1 != [] and dato2 != []:
                    self.acceder_ventana_dos()
        else:
            self.indica1['text'] = 'Usuario incorrecto'
            self.indica2['text'] = "Contraseña incorrecta"
            
    def widgets(self):
        self.logo = PhotoImage("optima.png")
        Label(self.master, image= self.logo, bg= "#34888C", height= 150, width= 150).pack()
        Label(self.master, text= 'Usuario', bg= "#34888C", fg= "black", font= ("Lucida Sans", 16, "bold")).pack(pady=5)
        self.entry1 = Entry(self.master, font= ("Comic Sans MS", 12), justify= "center", fg= "grey", highlightbackground= "#E65561", highlightcolor= "green2", highlightthickness= 5)
        self.entry1.insert(0, self.user_marcar)
        self.entry1.bind("<FocusIn>", lambda args: self.entry_in(self.entry1))
        self.entry1.bind("<FocusOut>", lambda args: self.entry_out(self.entry1, self.user_marcar))
        self.entry1.pack(pady= 5)
        
        self.indica1 = Label(self.master, bg= "#34888C", fg= "black", font= ("Arial", 8, "bold"))
        self.indica1.pack(pady= 5)
        
        Label(self.master, text= 'Contraseña', bg= "#34888C", fg= "black", font= ("Lucida Sans", 16, "bold")).pack(pady=5)
        self.entry2 = Entry(self.master, font= ("Comic Sans MS", 12), justify= "center", fg= "grey", highlightbackground= "#E65561", highlightcolor= "green2", highlightthickness= 5)
        self.entry2.insert(0, self.contra_marcar)
        self.entry2.bind("<FocusIn>", lambda args: self.entry_in(self.entry2))
        self.entry2.bind("<FocusOut>", lambda args: self.entry_out(self.entry2, self.contra_marcar))
        self.entry2.pack(pady= 5)
        
        self.indica2 = Label(self.master, bg= "#34888C", fg= "black", font= ("Arial", 8, "bold"))
        self.indica2.pack(pady= 5)
        
        Button(self.master, text= "Iniciar sesión", command= self.verifica_user, background= "#E3B01E", font= ("Arial", 12, "bold")).pack(pady= 5)
        
        self.barra = ttk.Progressbar(self.master, length= 200, mode= "determinate", maximum= 100, orient= HORIZONTAL)
        self.barra.pack()
    
        Button(self.master, text= "Salir", bg= "#34888C", activebackground= "#34888C", bd= 0, fg= "black", font= ("Lucida Sans", 15, "italic"), command= self.salir).pack(pady=10)

ventana = Tk()
ventana.configure(bg= "#34888C")
ventana.geometry("350x500+500+50")
ventana.overrideredirect(1)
ventana.resizable(0,0)

app = Login(ventana)
app.mainloop() 
        