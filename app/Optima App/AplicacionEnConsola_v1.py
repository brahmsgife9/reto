### IMPORTS
import AuthUtils
import os
import pandas as pd

#-----------------------------------------------------

#region Notas
"""
▄▀█   █▀▀ ▄▀█ █▀▄▀█ █▄▄ █ ▄▀█ █▀█
█▀█   █▄▄ █▀█ █░▀░█ █▄█ █ █▀█ █▀▄

* Sistema de directorios y de base de datos principalmente :) --solucion--> Creacion de base de datos
* No inicializar al admin cada que se corra la app --solucion--> Creacion de base de datos / Inicializador del sistema
* Mejorar la obtencion de llaves, relacionado al punto 1 --solucion--> Creacion de la base de dats
* Mejorar creacion de usuario --solucion--> Mejorar creacion de usuario
* Usuario == COMMON_NAME? --solucion--> Base de datos / Mejorar creacion de usuario
* Sistema de directorios --> eliminar pems al eliminar usuarios --solucion--> Base de datos
* Falta poner login --solucion-->  Base de datos / Creacion de pantalla de login
* Mejorar carga de llave privada --solucion--> Hacerlo lmao
* Manejo temporal de eliminados con excepciones lmao --solucion--> Hacerlo Lmao


█▀ █▀█ █░░ █░█ █▀▀ █ █▀█ █▄░█ █▀▀ █▀
▄█ █▄█ █▄▄ █▄█ █▄▄ █ █▄█ █░▀█ ██▄ ▄█

* Creacion de base de datos ----> Carlos ---> query para csv en admin
* Inicializador del sistema --- LISTO
* Mejorar creacion de usuario ----> LISTO
* Creacion de pantalla de login ---> Listo
* Mejorar carga de llave privada ---> LISTO
* Disenar esquema de excepciones (podria esperar a la implementacion en si)
* Revision de certificado <---



█▀▄▀█ █▀▀█ █▀▄▀█ █▀▀ █▀▀▄ ▀▀█▀▀ █▀▀█ 　 █▀▀ █▀▀█ █▀▀█ █░░ █▀▀█ █▀▀ 
█░▀░█ █░░█ █░▀░█ █▀▀ █░░█ ░░█░░ █░░█ 　 █░░ █▄▄█ █▄▄▀ █░░ █░░█ ▀▀█ 
▀░░░▀ ▀▀▀▀ ▀░░░▀ ▀▀▀ ▀░░▀ ░░▀░░ ▀▀▀▀ 　 ▀▀▀ ▀░░▀ ▀░▀▀ ▀▀▀ ▀▀▀▀ ▀▀▀

* (1) Modificar el guardado de llaves. Tanto privada como publica. TABLE USUARIO (?) -- admin y usuario // carpetas simuladas
* (2) Modificar Admin a Rol para el formulario de creación de usuario
* (3) guardar llave y certificado o solo certificado (?) 
* (4) Borrar bien los certificados y llave publica de la nube. --> por borrar el registro de la db en caso de que se pueda almacenar
* (5) widgets para abrir y guardar archivos
* (6) si no haces al primer usuario creado en la inicializacion se bloquea porque nadie es admin y solo admin puede crear usuarios 
* (nota) ¿Se debería de generar certificados para el administrador original? --> depende de si el va a firmar cosas. Los admins firmaran?
    Si se intenta verificar con el administrador original, no se podra porque no se emite certificado para el
* (inf) esquema de excepciones y esas mamadas
"""
#endregion

#-----------------------------------------------------

#region Provisional

# Base de datos e inicializacion de sistema ##### (1)
path = os.getcwd()+"\\reto\\app\\Modulos\\"
user_path = path+"SimulatedUserDrive\\"
cloud_path =  path+"SimulatedCloudDrive\\"

cols = ["Nombre", "Apellido", "User", "Password", "EstadoProvincia","Localidad", "Admin"] ### (2) --> estructura de db

def crearUsuario():
    data = []
    for col in cols:
        data += [[input(f"{col} ->")]]
    
    return dict(zip(cols, data))

if not os.path.exists(cloud_path+"database.csv"):
    print("_______INIT_______") 
    print("____Creacion del primer administrador____")
    data = crearUsuario()
    db = pd.DataFrame(data)#indice es la llave primaria ####(6)
    db.to_csv(cloud_path+"database.csv")
    data = dict(db.iloc[-1,:])
    
    AuthUtils.gen_rsa_keys(user_path, cloud_path, data) ##### (1)
    
## CARGA DE BASE DE DATOS
db = pd.read_csv(cloud_path+"database.csv", index_col= 0)

print("----BASE DE DATOS----\n", db)
#endregion

#-----------------------------------------------------

#region Pagina de Administrador
#region Subpaginas administrador
def creacionCertificado(subject, user_pub_key, issuer):
    print("___creacion de certificados___")
    admin_issuer = AuthUtils.gen_user(
        AuthUtils.gen_user_NameOID_dict(
            issuer))
    issuer_key = AuthUtils.read_key(user_path+issuer["Nombre"] +"_"+ issuer["Apellido"] +"_private_key.pem", password = issuer["User"]+issuer["Password"])
    expiry = int(input("Expiración del certificado en días ->"))
    certificado = AuthUtils.gen_certificado(subject, admin_issuer,user_pub_key, issuer_key, expiry, cloud_path) ### (3)
    AuthUtils.show_certificate(certificado)
    


def creacionLlaves(data, subject, issuer):
    print("___creacion de llaves___")
    # funcion de creacion de llaves
    user_pub_key = AuthUtils.gen_rsa_keys(user_path, cloud_path, data) ##### (1)

    creacionCertificado(subject, user_pub_key, issuer)


def creacionUsuario(index):  #YA xd
    print("___registrar login___")
    data = crearUsuario()
    
    db = pd.read_csv(cloud_path+"database.csv", index_col= 0)
    db = pd.concat([db, pd.DataFrame(data)], ignore_index= True)
    db.to_csv(cloud_path+"database.csv")

    data = dict(db.iloc[-1,:])

    #Consigue usuario:
    subject = AuthUtils.gen_user(AuthUtils.gen_user_NameOID_dict(data))
    #Consigue issuer:
    issuer = dict(db.loc[index])

    #GUARDAR informacion de login en base de datos   
    creacionLlaves(data, subject, issuer)
    
    print("Usuario Registrado Exitosamente!!")
    
 
def eliminacionUsuario(): ##### (4) #YA xd
    #Eliminacion de la entrada del login segun nombre
    print("___eliminar login___")
    db = pd.read_csv(cloud_path+"database.csv", index_col= 0)
    print(db)
    indice = int(input("indice->"))
    usuario = db.loc[indice]
    #Eliminacion de base de datos
    db = db.drop(indice)
    db.to_csv(cloud_path+"database.csv")
    
    #Eliminacion de la llave publica
    AuthUtils.delete_pem(cloud_path+usuario["Nombre"]+"_"+usuario["Apellido"]+"_public_key.pem")
    #Eliminacion del certificado
    AuthUtils.delete_pem(cloud_path+usuario["Nombre"]+"_"+usuario["Apellido"]+"_certificate.pem")
    print("Usuario eliminado exitosamente")

#endregion
def paginaAdmin(index):
    while True:    
        print("________ADMIN________")
        print("Crear Usuario ---------------1") # Creacion de login, llave y certificado
        print("Eliminar Usuario ------------2") # Eliminacion de login, llave y certificado
        
        el = input("->")
        
        if el == "1":
            creacionUsuario(index)
        elif el == "2":
            eliminacionUsuario()
        else:
            print("Saliendo....")
            break
#endregion

#------------------------------------------------------

#region Pagina Usuario
#region Subpaginas Usuario
def Firmar(index):
    
    db = pd.read_csv(cloud_path+"database.csv", index_col= 0)
    user = dict(db.loc[index])
    
    print("Cargando archivo...") #### (5)
    with open(path+"Cryptography and Network Security.pdf", "rb") as fp:
        to_sign = fp.read()
    
    
    key = AuthUtils.read_key(user_path+user["Nombre"] +"_"+ user["Apellido"] +"_private_key.pem", user["User"]+user["Password"])
    
    signature = AuthUtils.Firmar_Verificar(key, to_sign, signed = None)
    
    #Guarda en el drive la firma ####(5)
    with open(path+"signature.txt", "wb") as fp:
        fp.write(signature)
    

def val_certificate(nombre):
    pass


def Verificar():                    #### LA CARGA DE ARCHIVOS SE HARIA CON UN WIDGET EN LA GUI
    db = pd.read_csv(cloud_path+"database.csv", index_col= 0) ####(5)
    print(db) ###----> dropdown de los certificados
    
    indice_v = int(input("indice del usuario a verificar ->"))
    
    user_v =  dict(db.loc[indice_v])
    
    
    #### CERTIFICAOD OBTENIDO CON EL MENU DROPDOWN
    
    print("Validando vigencia del certificado....") ####(5)
    certificate = AuthUtils.load_certificate(cloud_path+user_v["Nombre"]+"_"+user_v["Apellido"]+"_certificate.pem")
    if not AuthUtils.val_certificate(certificate):
        print("Certificado expirado")
        return
    
    print("Carga de archivo...") ## CARGAR EL ARCHIVO CON UN WIDGET ####(5)
    with open(path+"Cryptography and Network Security.pdf", "rb") as fp:
        to_sign = fp.read()
    
    
    print("Carga archivo firma...") ### CARGAR EL ARCHIUVO CON UN WIDGET #####(5)
    with open(path+"signature.txt", "rb") as fp:
        signed = fp.read()
    
    key = AuthUtils.read_key(cloud_path+user_v["Nombre"] +"_"+ user_v["Apellido"] +"_public_key.pem", password = None) ## LO OBTIENTES DEL CERTIFICADO
    
    verified = AuthUtils.Firmar_Verificar(key, to_sign,signed)
    
    if verified:
        print("Verificacion exitosa!!!")
    else:
        print("Verificacion no exitosa!")
    
    
#endregion
def paginaUsuario(index):  
    while True:
        print("_______USUARIO______")
        print("Firmar ---------------1") 
        print("Verificar ------------2")
        
        el = input('->')
        
        if el == "1":
            Firmar(index)
        elif el == "2":
            Verificar()
        else:
            print("Cerrando....")
            break
#endregion

#-----------------------------------------------------

#region Menu Principal

def Login():
    usuario = input("Usuario ->")
    password = input("Password ->")
    users = list(db["User"]) 
    
    if usuario in users and list(db[db["User"] == usuario]["Password"])[0] == password:
        index = db[db["User"]== usuario]["Admin"].index[0] ### (2)
        if list(db[db["User"]== usuario]["Admin"])[0]:
            return "a", index
        else:
            return "u", index
    else:
        return "e", None
  
        
while True:
    
    print("_____________________")
    print("        LOGIN        ")
    print("_____________________")
    salir = input("Iniciar sesion? s/n")
    
    if salir == "s":
        el, index = Login()
        
        if el == "a":
            paginaAdmin(index)
        elif el == "u":
            paginaUsuario(index)
        elif el == "e":
            print("usuario y contraseña inválidos")
        else:
            break
    else:
        break
    
print("Cerrando....")
#endregion

