from telnetlib import VT3270REGIME
from tkinter import *
from tkinter import messagebox
import os
from turtle import Vec2D
import mysql.connector
import AuthUtils



### FUNCIONES DE OPTIMA
def sign_data():
    pass

def verifier_data():
    pass

def salir(window):
    window.withdraw()



### FUNCIONES DE LA APLICACIÓN

def sign():
    # Firmar 
    global nom, aps, uname, pswrd, edo, loc, rol
    
    v2 = Toplevel()
    v2.title('Firmar')
    v2.geometry("440x440")
    v2.configure(bg = "#C2E5E5")
    v2.iconbitmap(r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\optima.ico")
    
    # C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png
    image = PhotoImage(file=r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png")
    label = Label(v2, image=image)
    label.pack(pady=10)

    userR = Label(v2, text= "Firmar documento", bg= "#C2E5E5", font= ("Pointer One", 14))
    userR.pack(pady= 5)
    
    ## Eliminación
    ## Username
    e1 = Label(v2, text="Username", bg="#34888C", fg="white")
    e1.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    uname = Entry(v2)
    uname.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
 
    ## BOTONES
    ## Eliminar
    b = Button(v2, text="Firmar", command=lambda:[sign_data(), salir(v2)], background= "#E3B01E", font= ("Pointer One", 12))
    b.pack(pady=10)

def verifier():
    # Firmar 
    global nom, aps, uname, pswrd, edo, loc, rol
    
    v3 = Toplevel()
    v3.title('Verificar')
    v3.geometry("440x440")
    v3.configure(bg = "#C2E5E5")
    v3.iconbitmap(r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\optima.ico")
    
    # C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png
    image = PhotoImage(file=r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png")
    label = Label(v3, image=image)
    label.pack(pady=10)

    userR = Label(v3, text= "Verificar", bg= "#C2E5E5", font= ("Pointer One", 14))
    userR.pack(pady= 5)
    
    ## Eliminación
    ## Username
    e1 = Label(v3, text="Username", bg="#34888C", fg="white")
    e1.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    uname = Entry(v3)
    uname.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
 
    ## BOTONES
    ## Eliminar
    b = Button(v3, text="Verificar", command=lambda:[verifier_data(), salir(v3)], background= "#E3B01E", font= ("Pointer One", 12))
    b.pack(pady=10)

def user_app():
    root = Tk()
    root.geometry("440x440")
    root.title("User")
    root.configure(bg = "#C2E5E5")
    root.iconbitmap(r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\optima.ico")

    image = PhotoImage(file=r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png")
    label = Label(image=image, bg= "#C2E5E5")
    label.pack()

    user = Label(root, text= "USUARIO", bg= "#C2E5E5", font= ("Pointer One", 16))
    user.pack(pady= 20)
    Button(root, text= "Firmar", command=sign, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=15)
    Button(root, text= "Verificar", command=verifier, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=15)

    mainloop()



### APLICACIÓN
# Fondo C2E5E5
# Entry 34888C
# Botones E3B01E

root = Tk()
root.geometry("440x440")
root.title("User")
root.configure(bg = "#C2E5E5")
root.iconbitmap(r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\optima.ico")

image = PhotoImage(file=r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png")
label = Label(image=image, bg= "#C2E5E5")
label.pack()

user = Label(root, text= "USUARIO", bg= "#C2E5E5", font= ("Pointer One", 16))
user.pack(pady= 20)
Button(root, text= "Firmar", command=sign, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=15)
Button(root, text= "Verificar", command=verifier, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=15)

mainloop()
