"""
Firma


Verificiar


Certificar

"""
#region Imports
# Generacion de llaves
from re import L
from cryptography.hazmat.primitives.asymmetric import rsa
# Guardado y recuperacion de llaves
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends.openssl.rsa import _RSAPrivateKey, _RSAPublicKey
# Firmado
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
# Generacion de Certificados
from cryptography import x509
from cryptography.x509.oid import NameOID
import datetime
#endregion 


def read_key(path, password = None):
    with open(path, "rb") as key_file:
        if password != None:
            key = serialization.load_pem_private_key(key_file.read(), password = password.encode("utf-8")) 
        else:
            key = serialization.load_pem_public_key(key_file.read())
    return key


def write_pem(path, to_write):
    with open(path, "wb") as fp:
        fp.write(to_write)

def Firmar_Verificar(key, to_sign, signed = None):
    assert isinstance(to_sign, bytes), "to_sign debe de ser un objeto del tipo bytes"
    if isinstance(key, _RSAPrivateKey):
        return key.sign(
            to_sign, 
            padding.PSS(
                mgf = padding.MGF1(hashes.SHA256()),
                salt_length= padding.PSS.MAX_LENGTH
            ),
            hashes.SHA256())
    elif isinstance(key, _RSAPublicKey):
        assert isinstance(signed, bytes), "signed debe de ser un objeto del tipo bytes"
        try:
            key.verify(
                signed,
                to_sign,
                padding.PSS(
                    mgf=padding.MGF1(hashes.SHA256()),
                    salt_length=padding.PSS.MAX_LENGTH
                ),
                hashes.SHA256()
            )
            return True
        except:
            return False
    else:
        raise ValueError("key no es un tipo de llave aceptada")
    
def gen_rsa_keys(user_path, data, user, passw): ###### (1) (2)
    pr_k = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        )

    write_pem(user_path+"\\"+ data["Nombre"] +"_"+ data["Apellido"] +"_private_key.pem",
                        pr_k.private_bytes(
                        encoding = serialization.Encoding.PEM,
                        format = serialization.PrivateFormat.PKCS8,
                        encryption_algorithm=serialization.BestAvailableEncryption((user+passw).encode("utf-8"))
                        ))
    
    return pr_k.public_key()


def gen_user(data):
    
    attributes = {NameOID.COMMON_NAME: data["Nombre"]+"_"+data["Apellido"],
            NameOID.COUNTRY_NAME: "MX",#######(2)
            NameOID.STATE_OR_PROVINCE_NAME: data["EstadoProvincia"],
            NameOID.LOCALITY_NAME: data["Localidad"],
            NameOID.ORGANIZATION_NAME: "Fundación Teletón"}  
    
    attri = []
    for a in attributes:
        attri += [x509.NameAttribute(a, attributes[a])]    
    return x509.Name(attri)

def gen_certificado(subject, issuer, subject_key, issuer_key, expiry):
    certificate = (
        x509.CertificateBuilder()
        .subject_name(subject)
        .issuer_name(issuer)
        .public_key(subject_key)
        .serial_number(x509.random_serial_number())
        .not_valid_before(datetime.datetime.now())
        .not_valid_after(datetime.datetime.now() + datetime.timedelta(days=expiry))
        .sign(issuer_key, hashes.SHA256())
        )
    try:
        subject.get_attributes_for_oid(NameOID.COMMON_NAME)[0].value
    except:
        raise ValueError("subject debe de tener un atributo NameOID COMMON_NAME")
    
    return certificate.public_bytes(encoding = serialization.Encoding.PEM).decode('UTF-8')

def val_certificate(certificate):
    if datetime.datetime.now() < certificate.not_valid_after: #Vigente
        return True
    else:
        return False
        
def load_certificate(certificate):
    return x509.load_pem_x509_certificate(certificate.encode("UTF-8"))  
