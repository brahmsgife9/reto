from math import expm1
from telnetlib import VT3270REGIME
# from termios import VT1
from tkinter import *
from tkinter import messagebox
import os
import mysql.connector
import AuthUtils



path = os.getcwd()+"\\reto\\app\\Modulos\\"
user_path = path+"SimulatedUserDrive\\"
cloud_path =  path+"SimulatedCloudDrive\\"

cols = ["Nombre", "Apellido", "User", "Password", "EstadoProvincia","Localidad", "Admin"]



### FUNCIONES DE OPTIMA (no se pueden reutilizar código)

def insert_data():
    
    db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    cursor = db.cursor()
    sql = "INSERT INTO Usuario (nombre, apellido, username, password, estado, localidad, rol)\
       VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}')".format(nom.get(),aps.get(),uname.get(),pswrd.get(),edo.get(),loc.get(),rol.get())
    
    try:
        cursor.execute(sql)
        db.commit()
        nom.delete(0, 'end')
        aps.delete(0, 'end')
        uname.delete(0, 'end')
        pswrd.delete(0, 'end')
        edo.delete(0, 'end')
        loc.delete(0, 'end')
        rol.delete(0, 'end')
        messagebox.showinfo(message="Registro exitoso", title="Aviso")
        ### callback
    except:
        db.rollback()
        messagebox.showerror(message="No se pudo ejecutar el registro",title="Error fatal")
        
        db.close()

def delete_data():
    
    db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    cursor = db.cursor()
    sql = "DELETE FROM Usuario WHERE username='{0}'".format(uname.get)
    
    try:
        cursor.execute(sql)
        db.commit()
        # uname.delete(0, 'end')
        messagebox.showinfo(message="Baja de usuario exitosa", title="Aviso")
        ### callback
    except:
        db.rollback()
        messagebox.showerror(message="No se pudo eliminar el usuario, por favor asegúrese que el username sea correcto",title="Error fatal")
        
        db.close()

def salir(window):
    window.withdraw()



### FUNCIONES DE LA APLICACIÓN

def create_user():
    
    global nom, aps, uname, pswrd, edo, loc, rol
    
    v2 = Toplevel()
    v2.title('Registro')
    v2.geometry("440x750")
    v2.configure(bg = "#C2E5E5")
    v2.iconbitmap(r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\optima.ico")
    
    # C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png
    image = PhotoImage(file=r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png")
    label = Label(v2, image=image)
    label.pack(pady=10)

    userR = Label(v2, text= "Registro usuario", bg= "#C2E5E5", font= ("Pointer One", 14))
    userR.pack(pady= 5)
    
    ## REGISTRO
    ## Nombre
    e = Label(v2, text="Nombre(s)", bg="#34888C", fg="white")
    e.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    nom = Entry(v2)
    nom.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## Apellidos
    e1 = Label(v2, text="Apellidos", bg="#34888C", fg="white")
    e1.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    aps = Entry(v2)
    aps.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## Username
    e3 = Label(v2, text="Username", bg="#34888C", fg="white")
    e3.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    uname = Entry(v2)
    uname.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## Password
    e4 = Label(v2, text="Password", bg="#34888C", fg="white")
    e4.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    pswrd = Entry(v2)
    pswrd.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## Estado
    e5 = Label(v2, text="Estado", bg="#34888C", fg="white")
    e5.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    edo = Entry(v2)
    edo.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    
    ## Localidad
    e6 = Label(v2, text="Localidad", bg="#34888C", fg="white")
    e6.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    loc = Entry(v2)
    loc.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    
    ## Admin
    e7 = Label(v2, text="Rol", bg="#34888C", fg="white")
    e7.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    rol = Entry(v2)
    rol.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## BOTONES
    ## Registrar
    b = Button(v2, text="Registrar", command=lambda:[insert_data(), salir(v2)], background= "#E3B01E", font= ("Pointer One", 12))
    b.pack(pady=10)

    # CREACIÓN DE SU LLAVE PÚBLICA???
    # AuthUtils.gen_rsa_keys(user_path, cloud_path, data) data???

def delete_user():
    # Eliminación del usuario
    global nom, aps, uname, pswrd, edo, loc, rol
    
    v3 = Toplevel()
    v3.title('Exención')
    v3.geometry("440x440")
    v3.configure(bg = "#C2E5E5")
    v3.iconbitmap(r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\optima.ico")
    
    # C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png
    image = PhotoImage(file=r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png")
    label = Label(v3, image=image)
    label.pack(pady=10)

    userR = Label(v3, text= "Exención usuario", bg= "#C2E5E5", font= ("Pointer One", 14))
    userR.pack(pady= 5)
    
    ## Eliminación
    ## Username
    e1 = Label(v3, text="Username", bg="#34888C", fg="white")
    e1.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    uname = Entry(v3)
    uname.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
 
    ## BOTONES
    ## Eliminar
    b = Button(v3, text="Eliminar", command=lambda:[delete_data(), salir(v3)], background= "#E3B01E", font= ("Pointer One", 12))
    b.pack(pady=10)
    
    # Eliminación Llave pública del usuario???

def export_data():
    pass 

def sign():
    pass

def verifier():
    pass

def certificate(subject, user_pub_key, issuer):
    print("___creacion de certificados___")
    admin_issuer = AuthUtils.gen_user(AuthUtils.gen_user_NameOID_dict(issuer))
    issuer_key = AuthUtils.read_key(user_path+issuer["Nombre"] +"_"+ issuer["Apellido"] +"_private_key.pem", password = issuer["User"]+issuer["Password"])
    expiry = int(input("Expiración del certificado en días ->"))
    certificado = AuthUtils.gen_certificado(subject, admin_issuer,user_pub_key, issuer_key, expiry, cloud_path) ### (3)
    AuthUtils.show_certificate(certificado)



### FUNCION QUE LLAMA A LA APLICACIÓN 
def admin_app():
    
    root = Tk()
    root.geometry("440x540")
    root.title("Admin")
    root.configure(bg = "#C2E5E5")
    root.iconbitmap(r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\optima.ico")

    image = PhotoImage(file=r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png")
    label = Label(image=image, bg= "#C2E5E5")
    label.pack()

    user = Label(root, text= "ADMINISTRADOR", bg= "#C2E5E5", font= ("Pointer One", 16))
    user.pack(pady= 5)

    Button(root, text= "Crear usuario", command=create_user, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
    Button(root, text= "Eliminar usuario", command=delete_user, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
    Button(root, text= "Exportar datos", command=export_data, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
    Button(root, text= "Firmar", command=sign, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
    Button(root, text= "Verificar", command=verifier, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
    Button(root, text= "Certificado", command=certificate, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)

    mainloop()



### APLICACIÓN
# Fondo C2E5E5
# Entry 34888C
# Botones E3B01E
root = Tk()
root.geometry("440x540")
root.title("Admin")
root.configure(bg = "#C2E5E5")
root.iconbitmap(r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\optima.ico")

image = PhotoImage(file=r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png")
label = Label(image=image, bg= "#C2E5E5")
label.pack()

user = Label(root, text= "ADMINISTRADOR", bg= "#C2E5E5", font= ("Pointer One", 16))
user.pack(pady= 5)

Button(root, text= "Crear usuario", command=create_user, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
Button(root, text= "Eliminar usuario", command=delete_user, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
Button(root, text= "Exportar datos", command=export_data, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
Button(root, text= "Firmar", command=sign, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
Button(root, text= "Verificar", command=verifier, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
Button(root, text= "Certificado", command=certificate, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)

mainloop()
