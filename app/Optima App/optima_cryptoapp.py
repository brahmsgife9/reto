from tkinter import *
from tkinter import messagebox
import os
import mysql.connector
from math import expm1
from telnetlib import VT3270REGIME

from numpy import imag
# from termios import VT1
import AuthUtils
import csv
from tkinter import filedialog

#region Imports
# Generacion de llaves
from re import L
from cryptography.hazmat.primitives.asymmetric import rsa
# Guardado y recuperacion de llaves
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends.openssl.rsa import _RSAPrivateKey, _RSAPublicKey
# Firmado
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
# Generacion de Certificados
from cryptography import x509
from cryptography.x509.oid import NameOID
import datetime
import bases_datos as bases_datos
#endregion 


### FUNCIONES SQL
def search_user(user):

    db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    mycursor = db.cursor()
    sql = "SELECT username FROM Usuario WHERE username = '{}'".format(user)
    mycursor.execute(sql)
    userx = mycursor.fetchall()
    mycursor.close()

    if userx == []:
        return 0
    else:
        userx = userx[0][0]
        return userx

def search_pass(user, pswd):

    db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    mycursor = db.cursor()
    sql = "SELECT password FROM Usuario WHERE username = '{}' AND password = '{}'".format(user, pswd)
    mycursor.execute(sql)
    passx = mycursor.fetchall()
    mycursor.close()

    if passx == []:
        return 0
    else:
        passx = passx[0][0]
        return passx.encode('ISO-8859-1')   





### FUNCIONES DE LA APLICACIÓN
def entry_out(event, event_text):
    if event['fg'] == 'black' and len(event.get()) == 0:
        event.delete(0, END)
        event['fg'] = 'grey'
        event.insert(0, event_text)
            
    if entry_pass.get() != 'Ingresa tu contraseña':
        entry_pass['show'] = ""
        
    if entry_pass.get() != 'Ingresa tu usuario':
        entry_pass['show'] = ""
        
def entry_in(event):
    if event['fg'] == 'grey':
        event['fg'] = 'black'
        event.delete(0, END)
        
    if entry_pass.get() != 'Ingresa tu contraseña':
        entry_pass['show'] = ""
            
    if entry_pass.get() != 'Ingresa tu contraseña':
        entry_pass['show'] = "*"

def verifica_user():
    
    global users_entry, password_entry
    
    indica_user['text'] = ''
    indica_pass['text'] = ''
    users_entry = entry_user.get()
    password_entry = entry_pass.get()
    
    check = entry_pass.get().encode("UTF-8")
    digest = hashes.Hash(hashes.SHA256())
    digest.update(check)
    hash_check = digest.finalize()
        
    dato1 = search_user(users_entry)
    dato2 = search_pass(users_entry, hash_check.decode('ISO-8859-1'))
        
    if dato1 == users_entry and dato2 == hash_check:
        check_admin(users_entry)
    elif dato1 == users_entry and dato2 != hash_check:
        indica_pass['text'] = 'Contraseña incorrecta'
    else: 
        indica_pass['text'] = 'Usuario incorrecto'
    
def check_admin(username):
        db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
        mycursor = db.cursor()
        sql = "SELECT * FROM Usuario WHERE username = '{}'".format(username)
        mycursor.execute(sql)
        user = mycursor.fetchall()
        
        role= user[0][6]
        
        if role == 'admin':
            admin_window()
        else:
            user_window()
        
def salir(window):
    window.withdraw()





### FUNCIONALIDAD ADMIN
def insert_data():
    
    db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    cursor = db.cursor()
    
    prehash = pswrd.get().encode("UTF-8")
    digest = hashes.Hash(hashes.SHA256())
    digest.update(prehash)
    hash_pass = digest.finalize().decode('ISO-8859-1')
    
    sql = "INSERT INTO Usuario (nombre, apellido, username, password, estado, localidad, rol)\
       VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}')".format(nom.get(),aps.get(),uname.get(),hash_pass,edo.get(),loc.get(),rol.get())
    
    try:
        cursor.execute(sql)
        db.commit()
        nom.delete(0, 'end')
        aps.delete(0, 'end')
        uname.delete(0, 'end')
        pswrd.delete(0, 'end')
        edo.delete(0, 'end')
        loc.delete(0, 'end')
        rol.delete(0, 'end')
        expiry.delete(0, 'end')
        messagebox.showinfo(message="Registro exitoso", title="Aviso")
        ### callback
    except:
        db.rollback()
        messagebox.showerror(message="No se pudo ejecutar el registro",title="Error fatal")
        
        db.close()

def delete_data():
    
    db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    cursor = db.cursor()
    sql = "DELETE FROM Usuario WHERE nombre='{0}' AND apellido = '{1}'".format(nom.get(), aps.get())
    sql2 = "DELETE FROM Authutils WHERE nombre='{0}' AND apellido = '{1}'".format(nom.get(), aps.get())
    try:
        cursor.execute(sql)
        cursor.execute(sql2)
        db.commit()
        # uname.delete(0, 'end')
        messagebox.showinfo(message="Baja de usuario exitosa", title="Aviso")
        ### callback
    except:
        db.rollback()
        messagebox.showerror(message="No se pudo eliminar el usuario, por favor asegúrese que el username sea correcto",title="Error fatal")
        
        db.close()





### FUNCIONALIDAD USER
def sign_data(files):
    
    priv_key = AuthUtils.read_key(files["key_file"], users_entry+password_entry)
    
    with open(files["file"], "rb") as fp:
        to_sign = fp.read()
        
    path = filedialog.asksaveasfilename(title= "Guardar firma generada:")
    
    signature = AuthUtils.Firmar_Verificar(priv_key,to_sign,None)
    
    with open(path,"wb") as fp:
        fp.write(signature)
    
    messagebox.showinfo(message="Firma del archivo exitosa", title="Éxito")
    
    

def verifier_data(button_data, clicked, user_dict):
    cert= user_dict[clicked.get()]
    
    to_sign = button_data["signed_file"]
    
    signed = button_data["signature"]
    
    cert_loaded = AuthUtils.load_certificate(cert)
         
    if not AuthUtils.val_certificate(cert_loaded):
        messagebox.showerror(message="Certificado no válido o caduco", title="Certificado inválido")
        return 
    else:
        with open(to_sign, "rb") as fp:
            to_sign_file = fp.read()
            
        with open(signed, "rb") as fp:
            signed_file = fp.read()
            
        
        if AuthUtils.Firmar_Verificar(cert_loaded.public_key(), to_sign_file, signed_file):
            messagebox.showinfo(message="Verificación exitosa", title= "Éxito")
        else:
            messagebox.showerror(message="Verificación fallida", title= "Error fatal")
        





### FUNCIONES ADMIN
def create_user():
    
    global nom, aps, uname, pswrd, edo, loc, rol, expiry
    
    v1_1 = Toplevel()
    v1_1.title('Registro')
    v1_1.geometry("440x750")
    v1_1.configure(bg = "#C2E5E5")
    v1_1.iconbitmap(r"C:\Users\carav\OneDrive\Escritorio\Optima App\optima.ico")
    
    label = Label(v1_1, image=image, bg= "#C2E5E5")
    label.pack(pady=10)

    userR = Label(v1_1, text= "Registro usuario", bg= "#C2E5E5", font= ("Pointer One", 14))
    userR.pack(pady= 5)
    
    ## REGISTRO
    ## Nombre
    e = Label(v1_1, text="Nombre(s)", bg="#34888C", fg="white")
    e.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    nom = Entry(v1_1)
    nom.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## Apellidos
    e1 = Label(v1_1, text="Apellidos", bg="#34888C", fg="white")
    e1.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    aps = Entry(v1_1)
    aps.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## Username
    e3 = Label(v1_1, text="Username", bg="#34888C", fg="white")
    e3.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    uname = Entry(v1_1)
    uname.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## Password
    e4 = Label(v1_1, text="Password", bg="#34888C", fg="white")
    e4.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    pswrd = Entry(v1_1)
    pswrd.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## Estado
    e5 = Label(v1_1, text="Estado", bg="#34888C", fg="white")
    e5.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    edo = Entry(v1_1)
    edo.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    
    ## Localidad
    e6 = Label(v1_1, text="Localidad", bg="#34888C", fg="white")
    e6.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    loc = Entry(v1_1)
    loc.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    
    ## Admin
    e7 = Label(v1_1, text="Rol", bg="#34888C", fg="white")
    e7.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    rol = Entry(v1_1)
    rol.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    
    ##Expiración
    e8 = Label(v1_1, text="Duración", bg="#34888C", fg="white")
    e8.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    expiry = Entry(v1_1)
    expiry.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
        
    b = Button(v1_1, text="Registrar", width=10, anchor="center", command=lambda:[certificado(), insert_data(), salir(v1_1)], background= "#E3B01E", font= ("Pointer One", 12))
    b.pack(pady=10)


def certificado():
    
    db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
        )
    
    mycursor = db.cursor()
        
    user_attr = {
            'Nombre' : nom.get(),
            'Apellido' : aps.get(),
            'EstadoProvincia': edo.get(),
            'Localidad' : loc.get(),
        }
        
    user = AuthUtils.gen_user(user_attr)
        
    sql = "SELECT * FROM Usuario WHERE username = '{0}'".format(users_entry)
    mycursor.execute(sql)
    adm_info = mycursor.fetchall()
    adm_info = adm_info[0]
        
    adm_attr ={
        'Nombre' : adm_info[0],
        'Apellido' : adm_info[1],
        'EstadoProvincia': adm_info[4],
        'Localidad' : adm_info[5],
    }

    issuer = AuthUtils.gen_user(adm_attr)
        
    pr_k = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        )
    
    user_pub_key = pr_k.public_key().public_bytes(
        encoding = serialization.Encoding.PEM,
        format = serialization.PublicFormat.SubjectPublicKeyInfo
        ).decode('UTF-8')

    path = filedialog.askdirectory()
    
    user_pub_key = AuthUtils.gen_rsa_keys(path, user_attr, uname.get(), pswrd.get())
    
    adm_key_path = filedialog.askopenfilename(initialdir= "*",
                                               title= "Seleccione su llave",
                                               filetypes=(("pem files","*.pem"), ("All Files","*.*")))
    
    adm_key = AuthUtils.read_key(adm_key_path, password= users_entry + password_entry)
    certificate_bytes = AuthUtils.gen_certificado(user, issuer, user_pub_key, adm_key, int(expiry.get()))
    
    sql2 = "INSERT INTO Authutils (nombre, apellido, certificate) VALUES ('{0}', '{1}', '{2}')".format(nom.get(), aps.get(), certificate_bytes)
    mycursor.execute(sql2)
    db.commit()
    db.close()


def delete_user():
    # Eliminación del usuario
    global nom, aps, uname, pswrd, edo, loc, rol
     
    v1_2 = Toplevel()
    v1_2.title('Exención')
    v1_2.geometry("440x360")
    v1_2.configure(bg = "#C2E5E5")
    v1_2.iconbitmap(r"C:\Users\carav\OneDrive\Escritorio\Optima App\optima.ico")
    
    label = Label(v1_2, image=image, bg= "#C2E5E5")
    label.pack(pady=10)

    userR = Label(v1_2, text= "Exención usuario", bg= "#C2E5E5", font= ("Pointer One", 14))
    userR.pack(pady= 5)
    
    ## Eliminación
    ## Username
    e1 = Label(v1_2, text="Nombre", bg="#34888C", fg="white")
    e1.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    nom = Entry(v1_2)
    nom.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    
    e2 = Label(v1_2, text="Apellido", bg="#34888C", fg="white")
    e2.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    aps = Entry(v1_2)
    aps.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
 
    ## BOTONES
    ## Eliminar
    b = Button(v1_2, text="Eliminar", width=10, anchor="center", command=lambda:[delete_data(), salir(v1_2)], background= "#E3B01E", font= ("Pointer One", 12))
    b.pack(pady=10)


def export_data():
    db = mysql.connector.connect(
        host="localhost",
        user= "root",
        passwd= "root",
        database="testdatabase"
    )

    cursor = db.cursor()
    cursor.execute("SELECT * FROM Usuario")
    rows = cursor.fetchall()

    csv_fields = ["NOMBRE", "APELLIDO", "USERNAME", "PASSWORD",
    "ESTADO", "LOCALIDAD", "ROL", "ID"]

    csv_rows = []

    for row in rows:
        row = list(row)
        csv_rows.append(row)

    path = filedialog.asksaveasfilename(title= "Guardar datos:", filetypes=[("CSV Files", ".csv")])
    path+=".csv"
    with open(path, 'w', encoding='ISO-8859-1') as f:
        write = csv.writer(f)

        write.writerow(csv_fields)
        write.writerows(csv_rows)

    db.close()

    messagebox.showinfo(message="Se han importado los datos con éxito", title= "Exportación exitosa")



### FUNCIONES USER
def sign():
    # Firmar 
    global nom, aps, uname, pswrd, edo, loc, rol
    
    v2_1 = Toplevel()
    v2_1.title('Firmar')
    v2_1.geometry("440x440")
    v2_1.configure(bg = "#C2E5E5")
    v2_1.iconbitmap(r"C:\Users\carav\OneDrive\Escritorio\Optima App\optima.ico")
    
    # C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png
    label = Label(v2_1, image=image, bg= "#C2E5E5")
    label.pack(pady=10)

    userR = Label(v2_1, text= "Firmar documento", bg= "#C2E5E5", font= ("Pointer One", 14))
    userR.pack(pady= 5)
    
    button_data = {}
    
    ## Firmar

    e1 = Label(v2_1, text="Llave privada", bg="#34888C", fg="white")
    e1.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    b1 = Button(v2_1, text="Seleccione archivo", width=20, anchor="center", command=lambda:[priv_key_select(button_data)], background= "#E3B01E", font= ("Pointer One", 12))
    b1.pack(pady=10)

    ## Archivo

    e2 = Label(v2_1, text="Archivo", bg="#34888C", fg="white")
    e2.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    b2 = Button(v2_1, text="Seleccione archivo", width=20, anchor="center", command=lambda:[to_sign_file(button_data)], background= "#E3B01E", font= ("Pointer One", 12))
    b2.pack(pady=10)
 
    b = Button(v2_1, text="Firmar", width=10, anchor="center", command=lambda:[sign_data(button_data), salir(v2_1)], background= "#E3B01E", font= ("Pointer One", 12))
    b.pack(pady=10)

def priv_key_select(data):
    priv_key_file = filedialog.askopenfilename(initialdir= "*",
                                               title= "Select File",
                                               filetypes=(("pem files", "*.pem"), ("All Files","*.*")))
    messagebox.showinfo(message= "Su llave privada es: " + str(priv_key_file), title="Llave privada")
    data["key_file"] = priv_key_file

def to_sign_file(data):
    to_sign = filedialog.askopenfilename(initialdir= "*",
                                               title= "Select File",
                                               filetypes=(("pdf files","*.pdf"),("xml files","*.xml"), ("All Files","*.*")))
    messagebox.showinfo(message= "El archivo a firmar es: " + str(to_sign), title="Archivo a firmar")
    data["file"] = to_sign

def verifier():
    db = mysql.connector.connect(
        host="localhost",
        user= "root",
        passwd= "root",
        database="testdatabase"
    )
    
    mycursor = db.cursor()
    # Firmar 
    global nom, aps, uname, pswrd, edo, loc, rol, clicked
    
    v2_2 = Toplevel()
    v2_2.title('Verificar')
    v2_2.geometry("440x490")
    v2_2.configure(bg = "#C2E5E5")
    v2_2.iconbitmap(r"C:\Users\carav\OneDrive\Escritorio\Optima App\optima.ico")
    
    # C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png
    label = Label(v2_2, image=image, bg= "#C2E5E5")
    label.pack(pady=10)

    userR = Label(v2_2, text= "Verificar", bg= "#C2E5E5", font= ("Pointer One", 14))
    userR.pack(pady= 5)
    
    button_data = {}
    ## Verificar
    ## Archivo
    e1 = Label(v2_2, text="Archivo", bg="#34888C", fg="white")
    e1.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    b1 = Button(v2_2, text="Seleccione archivo", width=20, anchor="center", command=lambda:[signed_file(button_data)], background= "#E3B01E", font= ("Pointer One", 12))
    b1.pack(pady=10)

    ## Firma
    e2 = Label(v2_2, text="Firma", bg="#34888C", fg="white")
    e2.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    b2 = Button(v2_2, text="Seleccione archivo", width=20, anchor="center", command=lambda:[signature(button_data)], background= "#E3B01E", font= ("Pointer One", 12))
    b2.pack(pady=10)

    ## Certificado
    e2 = Label(v2_2, text="Certificado", bg="#34888C", fg="white")
    e2.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    
    sql = "SELECT * FROM Authutils"
    mycursor.execute(sql)
    users = mycursor.fetchall()
    
    user_dict = {tup[0]+" "+tup[1]:tup[2] for tup in users}
    
    options = list(user_dict.keys())
    
    clicked = StringVar()
    drop = OptionMenu(v2_2, clicked, *options)
    drop.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5)

    
 
    ## BOTONES
    ## Eliminar
    b = Button(v2_2, text="Verificar", width=10, anchor="center", command=lambda:[verifier_data(button_data, clicked, user_dict), salir(v2_2)], background= "#E3B01E", font= ("Pointer One", 12))
    b.pack(pady=10)


def signed_file(data):
    signed_f = filedialog.askopenfilename(initialdir= "*",
                                               title= "Select File",
                                               filetypes=(("pdf files", "*.pdf"), ("All Files","*.*")))
    messagebox.showinfo(message= "Su archivo firmado es: " + str(signed_f), title="Archivo firmado")
    data["signed_file"] = signed_f

def signature(data):
    signature_ = filedialog.askopenfilename(initialdir= "*",
                                               title= "Select File")
    messagebox.showinfo(message= "Su firma es: " + str(signature_), title="Archivo de firma")
    data["signature"] = signature_
    


### FUNCIONES - ADMIN / USER
def admin_window():
    root.withdraw()
    v1 = Toplevel(root)
    v1.geometry("440x640")
    v1.title("Admin")
    v1.configure(bg = "#C2E5E5")
    v1.iconbitmap(r"C:\Users\carav\OneDrive\Escritorio\Optima App\optima.ico")

    label = Label(v1, image=image, bg= "#C2E5E5")
    label.pack()

    user = Label(v1, text= "ADMINISTRADOR", bg= "#C2E5E5", font= ("Pointer One", 16))
    user.pack(pady= 20)

    Button(v1, text= "Crear usuario", width=15, height=2, anchor="center", relief="raised", borderwidth=3, command=create_user, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
    Button(v1, text= "Eliminar usuario", width=15, height=2, anchor="center", relief="raised", borderwidth=3, command=delete_user, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
    Button(v1, text= "Exportar datos", width=15, height=2, anchor="center", relief="raised", borderwidth=3, command=export_data, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
    Button(v1, text= "Firmar", width=15, height=2, anchor="center", relief="raised", borderwidth=3, command=sign, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
    Button(v1, text= "Verificar", width=15, height=2, anchor="center", relief="raised", borderwidth=3, command=verifier, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=10)
    v1.protocol("WM_DELETE_WINDOW", on_closing)

def user_window():
    root.withdraw()
    v2 = Toplevel()
    # REGRESAR A 440x440
    v2.geometry("440x440")
    v2.title("User")
    v2.configure(bg = "#C2E5E5")
    v2.iconbitmap(r"C:\Users\carav\OneDrive\Escritorio\Optima App\optima.ico")

    label = Label(v2, image=image, bg= "#C2E5E5")
    label.pack()

    user = Label(v2, text= "USUARIO", bg= "#C2E5E5", font= ("Pointer One", 16))
    user.pack(pady= 20)
    Button(v2, text= "Firmar", width=15, height=2, anchor="center", relief="raised", borderwidth=3, command=sign, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=15)
    Button(v2, text= "Verificar", width=15, height=2, anchor="center", relief="raised", borderwidth=3, command=verifier, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=15)
    v2.protocol("WM_DELETE_WINDOW", on_closing)


def on_closing():
    if messagebox.askokcancel("Salir", "¿Desea salir de la aplicación?"):
        root.destroy()


### APLICACIÓN
# Fondo C2E5E5
# Entry 34888C
# Botones E3B01E

### CHEQUEO DE BASES DE DATOS EXISTENTES
db = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="root",
    database="testdatabase"
)
try:
    mycursor = db.cursor()
    sql = "SELECT * FROM USUARIO"
    mycursor.execute(sql)

except:

    bases_datos.inicializar_programa()

root = Tk()
root.geometry("440x440")
root.title("Optima App")
root.configure(bg = "#C2E5E5")
root.iconbitmap(r"C:\Users\carav\OneDrive\Escritorio\Optima App\optima.ico")

global image 
image = PhotoImage(file=r"C:\Users\carav\OneDrive\Escritorio\Optima App\blackopt.png")

label = Label(image=image, bg= "#C2E5E5")
label.pack()

user_marcar = "Ingresa tu usuario"

user = Label(root, text= "Usuario", bg= "#C2E5E5", font= ("Pointer One", 16))
user.pack(pady= 5)
entry_user = Entry(root, font= ("Maintree", 12), justify= "center", fg= "grey", highlightbackground= "#34888C", highlightcolor= "#296c70", highlightthickness= 3)
entry_user.insert(0, user_marcar)
entry_user.bind("<FocusIn>", lambda args: entry_in(entry_user))
entry_user.bind("<FocusOut>", lambda args: entry_out(entry_user, user_marcar))
entry_user.pack(pady=5)

indica_user = Label(root, bg= "#C2E5E5", fg= "black", font= ("Maintree", 8, "bold"))
indica_user.pack(pady= 5)

pass_marcar = "Ingresa tu contraseña"

pswd = Label(root, text= "Contraseña", bg= "#C2E5E5", font= ("Pointer One", 16))
pswd.pack(pady= 5)
entry_pass = Entry(root, font= ("Maintree", 12), justify= "center", fg= "grey", highlightbackground= "#34888c", highlightcolor= "#296c70", highlightthickness= 3)
entry_pass.insert(0, pass_marcar)
entry_pass.bind("<FocusIn>", lambda args: entry_in(entry_pass))
entry_pass.bind("<FocusOut>", lambda args: entry_out(entry_pass, pass_marcar))
entry_pass.pack(pady=5)

indica_pass = Label(root, bg= "#C2E5E5", fg= "black", font= ("Maintree", 8, "bold"))
indica_pass.pack(pady= 5)

Button(root, text= "Iniciar sesión", anchor="center", relief="raised", borderwidth=3, command= verifica_user, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=5)
root.protocol("WM_DELETE_WINDOW", on_closing)
mainloop()


