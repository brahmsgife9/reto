import sys
from cx_Freeze import setup, Executable
from multiprocessing import freeze_support
freeze_support()

# Dependencies are automatically detected, but it might need fine tuning.
# "packages": ["os"] is used as example only
build_exe_options = {"packages": ["os"], "include_files": ["optima.ico",
                                                           "blackopt.png",
                                                           "AuthUtils.py",
                                                           "bases_datos.py"]}

# base="Win32GUI" should be used only for Windows GUI app
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(
    name="Optima Crypto App",
    version="0.1",
    description="Verifique la autenticidad de sus archivos",
    options={"build_exe": build_exe_options},
    executables=[Executable("optima_cryptoapp.py", base=base)],
)