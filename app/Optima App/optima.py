from tkinter import *
from tkinter import messagebox
import mysql.connector



### FUNCIONES SQL

def search_user(user):

    db = mysql.connector.connect(
    
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    mycursor = db.cursor()
    sql = "SELECT username FROM Usuario WHERE username = '{}'".format(user)
    mycursor.execute(sql)
    userx = mycursor.fetchall()
    mycursor.close()
        
    return userx

def search_pass(pswd):

    db = mysql.connector.connect(
    
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    mycursor = db.cursor()
    sql = "SELECT password FROM Usuario WHERE password = '{}'".format(pswd)
    mycursor.execute(sql)
    passx = mycursor.fetchall()
    mycursor.close()
        
    return passx



### FUNCIONES DE LA APLICACIÓN

def entry_out(event, event_text):
    if event['fg'] == 'black' and len(event.get()) == 0:
        event.delete(0, END)
        event['fg'] = 'grey'
        event.insert(0, event_text)
            
    if entry_pass.get() != 'Ingresa tu contraseña':
        entry_pass['show'] = ""
        
    if entry_pass.get() != 'Ingresa tu usuario':
        entry_pass['show'] = ""
        
def entry_in(event):
    if event['fg'] == 'grey':
        event['fg'] = 'black'
        event.delete(0, END)
        
    if entry_pass.get() != 'Ingresa tu contraseña':
        entry_pass['show'] = ""
            
    if entry_pass.get() != 'Ingresa tu contraseña':
        entry_pass['show'] = "*"

def registro():

    global nom, aps, uname, pswrd, edo, loc, rol
    
    v2 = Toplevel()
    v2.title('Registro')
    v2.geometry("350x750")
    v2.configure(bg = "#34888C")
    v2.iconbitmap(r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\optima.ico")
    
    image = PhotoImage(file=r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png")
    label = Label(v2, image=image)
    label.pack()
    
    ## REGISTRO

    """
    #ID
    e0 = Label(v2, text="ID", bg="gray", fg="white")
    e0.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
    id1 = Entry(v2)
    id1.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
    """
    
    ## Nombre
    e = Label(v2, text="Nombre(s)", bg="gray", fg="white")
    e.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
    nom = Entry(v2)
    nom.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)

    ## Apellidos
    e1 = Label(v2, text="Apellidos", bg="gray", fg="white")
    e1.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
    aps = Entry(v2)
    aps.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)

    ## Username
    e3 = Label(v2, text="Username", bg="gray", fg="white")
    e3.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
    uname = Entry(v2)
    uname.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)

    ## Password
    e4 = Label(v2, text="Password", bg="gray", fg="white")
    e4.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
    pswrd = Entry(v2)
    pswrd.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)

    ## Estado
    e5 = Label(v2, text="Estado", bg="gray", fg="white")
    e5.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
    edo = Entry(v2)
    edo.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
    
    ## Localidad
    e6 = Label(v2, text="Localidad", bg="gray", fg="white")
    e6.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
    loc = Entry(v2)
    loc.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
    
    ## Rol
    e7 = Label(v2, text="Rol", bg="gray", fg="white")
    e7.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)
    rol = Entry(v2)
    rol.pack(pady=5, padx=5, ipadx=5, ipady=5, fill=X)

    ## BOTONES
    ## Registrar
    b = Button(v2, text="Registrar", fg="black", command=insert_data)
    b.pack()
    
def insert_data():
    
    db = mysql.connector.connect(
    
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    cursor = db.cursor()
    sql = "INSERT INTO Usuario (nombre, apellido, username, password, estado, localidad, rol)\
       VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}')".format(nom.get(),aps.get(),uname.get(),pswrd.get(),edo.get(),loc.get(),rol.get())
    
    try:
        cursor.execute(sql)
        db.commit()
        nom.delete(0, 'end')
        aps.delete(0, 'end')
        uname.delete(0, 'end')
        pswrd.delete(0, 'end')
        edo.delete(0, 'end')
        loc.delete(0, 'end')
        rol.delete(0, 'end')
        messagebox.showinfo(message="Registro exitoso", title="Aviso")
    except:
        db.rollback()
        messagebox.showerror(message="No se pudo ejecutar el registro",title="Error fatal")
        
        db.close()
    

def verifica_user():
    indica_user['text'] = ''
    indica_pass['text'] = ''
    users_entry = entry_user.get()
    password_entry = entry_pass.get()
        
    dato1 = search_user(users_entry)
    dato2 = search_pass(password_entry)
        
    fila1 = dato1
    fila2 = dato2
        
    if fila1 == fila2:
        if dato1 == [] and dato2 == []:
            indica_pass['text'] = "Contraseña incorrecta"
            indica_user['text'] = "Usuario incorrecto"
        else:
            if dato1 == []:
                indica_user['text'] = "Usuario incorrecto"
            else:
                dato1 = dato1[0][1]
            if dato2 == []:
                indica_pass['text'] = "Contraseña incorrecta"
            else:
                dato2 = dato2[0][2]
                
            if dato1 != [] and dato2 != []:
                # ventana_dos()
                pass
    else:
        indica_user['text'] = 'Usuario correcto'
        indica_pass['text'] = "Contraseña correcta"
    
    role = check_admin(users_entry)
    
def check_admin(username):
        db = mysql.connector.connect(
    
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
        mycursor = db.cursor()
        sql = "SELECT * FROM Usuario WHERE username = '{}'".format(username)
        mycursor.execute(sql)
        user = mycursor.fetchall()
        
        role= user[0][6]
        
        if role == 'admin':
            messagebox.showinfo(message='Vos sos ADMIN!!! la remil', title='AVISO')
            admin_window()
        else:
            user_window()
        
def admin_window():
    root.withdraw()
    import admin

def user_window():
    root.withdraw() 
    import user

def salir():
    root.destroy()



### FUNCIÓN QUE LLAMA A LA APLICACIÓN
def optima_app():
    root = Tk()
    root.geometry("440x440")
    root.title("Optima App")
    root.configure(bg = "#C2E5E5")
    root.iconbitmap(r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\optima.ico")

    image = PhotoImage(file=r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png")
    label = Label(image=image, bg= "#C2E5E5")
    label.pack()

    user_marcar = "Ingresa tu usuario"

    user = Label(root, text= "Usuario", bg= "#C2E5E5", font= ("Pointer One", 16))
    user.pack(pady= 5)
    entry_user = Entry(root, font= ("Maintree", 12), justify= "center", fg= "grey", highlightbackground= "#34888C", highlightcolor= "#296c70", highlightthickness= 3)
    entry_user.insert(0, user_marcar)
    entry_user.bind("<FocusIn>", lambda args: entry_in(entry_user))
    entry_user.bind("<FocusOut>", lambda args: entry_out(entry_user, user_marcar))
    entry_user.pack(pady=5)

    indica_user = Label(root, bg= "#C2E5E5", fg= "black", font= ("Maintree", 8, "bold"))
    indica_user.pack(pady= 5)

    pass_marcar = "Ingresa tu contraseña"

    pswd = Label(root, text= "Contraseña", bg= "#C2E5E5", font= ("Pointer One", 16))
    pswd.pack(pady= 5)
    entry_pass = Entry(root, font= ("Maintree", 12), justify= "center", fg= "grey", highlightbackground= "#34888c", highlightcolor= "#296c70", highlightthickness= 3)
    entry_pass.insert(0, pass_marcar)
    entry_pass.bind("<FocusIn>", lambda args: entry_in(entry_pass))
    entry_pass.bind("<FocusOut>", lambda args: entry_out(entry_pass, pass_marcar))
    entry_pass.pack(pady=5)

    indica_pass = Label(root, bg= "#C2E5E5", fg= "black", font= ("Maintree", 8, "bold"))
    indica_pass.pack(pady= 5)

    Button(root, text= "Iniciar sesión", command= verifica_user, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=5)

    mainloop()



### APLICACIÓN
# Fondo C2E5E5
# Entry 34888C
# Botones E3B01E

root = Tk()
root.geometry("440x440")
root.title("Optima App")
root.configure(bg = "#C2E5E5")
root.iconbitmap(r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\optima.ico")

image = PhotoImage(file=r"C:\Users\HP456787\Documents\ITESM\6TO SEM\MA2007B\reto\app\Reto_cripto\blackopt.png")
label = Label(image=image, bg= "#C2E5E5")
label.pack()

user_marcar = "Ingresa tu usuario"

user = Label(root, text= "Usuario", bg= "#C2E5E5", font= ("Pointer One", 16))
user.pack(pady= 5)
entry_user = Entry(root, font= ("Maintree", 12), justify= "center", fg= "grey", highlightbackground= "#34888C", highlightcolor= "#296c70", highlightthickness= 3)
entry_user.insert(0, user_marcar)
entry_user.bind("<FocusIn>", lambda args: entry_in(entry_user))
entry_user.bind("<FocusOut>", lambda args: entry_out(entry_user, user_marcar))
entry_user.pack(pady=5)

indica_user = Label(root, bg= "#C2E5E5", fg= "black", font= ("Maintree", 8, "bold"))
indica_user.pack(pady= 5)

pass_marcar = "Ingresa tu contraseña"

pswd = Label(root, text= "Contraseña", bg= "#C2E5E5", font= ("Pointer One", 16))
pswd.pack(pady= 5)
entry_pass = Entry(root, font= ("Maintree", 12), justify= "center", fg= "grey", highlightbackground= "#34888c", highlightcolor= "#296c70", highlightthickness= 3)
entry_pass.insert(0, pass_marcar)
entry_pass.bind("<FocusIn>", lambda args: entry_in(entry_pass))
entry_pass.bind("<FocusOut>", lambda args: entry_out(entry_pass, pass_marcar))
entry_pass.pack(pady=5)

indica_pass = Label(root, bg= "#C2E5E5", fg= "black", font= ("Maintree", 8, "bold"))
indica_pass.pack(pady= 5)

Button(root, text= "Iniciar sesión", command= verifica_user, background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=5)

mainloop()