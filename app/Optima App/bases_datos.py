from tkinter import *
from tkinter import messagebox
import os
import mysql.connector
from math import expm1
from telnetlib import VT3270REGIME

from numpy import imag
from pyparsing import matchOnlyAtCol
# from termios import VT1
import AuthUtils
import csv
from tkinter import filedialog

#region Imports
# Generacion de llaves
from re import L
from cryptography.hazmat.primitives.asymmetric import rsa
# Guardado y recuperacion de llaves
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends.openssl.rsa import _RSAPrivateKey, _RSAPublicKey
# Firmado
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
# Generacion de Certificados
from cryptography import x509
from cryptography.x509.oid import NameOID
import datetime
#endregion 

def insert_data():
    
    db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="testdatabase"
    )
    
    mycursor = db.cursor()
    prehash = pswrd.get().encode("UTF-8")
    digest = hashes.Hash(hashes.SHA256())
    digest.update(prehash)
    hash_pass = digest.finalize().decode('ISO-8859-1')
    
    sql = "INSERT INTO Usuario (nombre, apellido, username, password, estado, localidad, rol)\
       VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}')".format(nom.get(),aps.get(),uname.get(),hash_pass,edo.get(),loc.get(),rol.get())
    
    try:
        mycursor.execute(sql)
        db.commit()
        nom.delete(0, 'end')
        aps.delete(0, 'end')
        uname.delete(0, 'end')
        pswrd.delete(0, 'end')
        edo.delete(0, 'end')
        loc.delete(0, 'end')
        rol.delete(0, 'end')
        expiry.delete(0, 'end')
        messagebox.showinfo(message="Registro exitoso. Puede cerrar la ventana de creación", title="Aviso")
        ### callback
    except:
        db.rollback()
        messagebox.showerror(message="No se pudo ejecutar el registro",title="Error fatal")
        
        db.close()

def create_user():
    
    global nom, aps, uname, pswrd, edo, loc, rol, expiry
    
    v1_1 = Toplevel()
    v1_1.title('Registro')
    v1_1.geometry("440x750")
    v1_1.configure(bg = "#C2E5E5")
    v1_1.iconbitmap(r"C:\Users\carav\OneDrive\Escritorio\Optima App\optima.ico")
    
    label = Label(v1_1, image=image, bg= "#C2E5E5")
    label.pack(pady=10)

    userR = Label(v1_1, text= "Registro usuario", bg= "#C2E5E5", font= ("Pointer One", 14))
    userR.pack(pady= 5)
    
    ## REGISTRO
    ## Nombre
    e = Label(v1_1, text="Nombre(s)", bg="#34888C", fg="white")
    e.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    nom = Entry(v1_1)
    nom.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## Apellidos
    e1 = Label(v1_1, text="Apellidos", bg="#34888C", fg="white")
    e1.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    aps = Entry(v1_1)
    aps.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## Username
    e3 = Label(v1_1, text="Username", bg="#34888C", fg="white")
    e3.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    uname = Entry(v1_1)
    uname.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## Password
    e4 = Label(v1_1, text="Password", bg="#34888C", fg="white")
    e4.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    pswrd = Entry(v1_1)
    pswrd.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)

    ## Estado
    e5 = Label(v1_1, text="Estado", bg="#34888C", fg="white")
    e5.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    edo = Entry(v1_1)
    edo.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    
    ## Localidad
    e6 = Label(v1_1, text="Localidad", bg="#34888C", fg="white")
    e6.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    loc = Entry(v1_1)
    loc.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    
    ## Admin
    e7 = Label(v1_1, text="Rol", bg="#34888C", fg="white")
    e7.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    rol = Entry(v1_1)
    rol.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    
    ##Expiración
    e8 = Label(v1_1, text="Duración", bg="#34888C", fg="white")
    e8.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
    expiry = Entry(v1_1)
    expiry.pack(pady=2.5, padx=2.5, ipadx=2.5, ipady=2.5, fill=X)
        
    b = Button(v1_1, text="Registrar", width=10, anchor="center", command=lambda:[certificado(), insert_data(), salir(v1_1)], background= "#E3B01E", font= ("Pointer One", 12))
    b.pack(pady=10)

def certificado():
    db = mysql.connector.connect(

    host="localhost",
    user="root",
    passwd="root",
    database="testdatabase"
    )
    
    mycursor = db.cursor()
    
    user_attr = {
        'Nombre' : nom.get(),
        'Apellido' : aps.get(),
        'EstadoProvincia': edo.get(),
        'Localidad' : loc.get(),
    }
        
    user = AuthUtils.gen_user(user_attr)
        
    adm_attr ={
    'Nombre' : nom.get(),
    'Apellido' : aps.get(),
    'EstadoProvincia': edo.get(),
    'Localidad' : loc.get()
    }

    issuer = AuthUtils.gen_user(adm_attr)

    pr_k = rsa.generate_private_key(
    public_exponent=65537,
    key_size=2048,
    )

    user_pub_key = pr_k.public_key().public_bytes(
    encoding = serialization.Encoding.PEM,
    format = serialization.PublicFormat.SubjectPublicKeyInfo
    ).decode('UTF-8')

    path = filedialog.askdirectory()

    user_pub_key = AuthUtils.gen_rsa_keys(path, user_attr, uname.get(), pswrd.get())

    adm_key_path = filedialog.askopenfilename(initialdir= "*",
                                        title= "Seleccione su llave",
                                        filetypes=(("pem files","*.pem"), ("All Files","*.*")))

    adm_key = AuthUtils.read_key(adm_key_path, password= uname.get() + pswrd.get())
    certificate_bytes = AuthUtils.gen_certificado(user, issuer, user_pub_key, adm_key, int(expiry.get()))
    sql = "INSERT INTO Authutils (nombre, apellido, certificate) VALUES ('{0}', '{1}', '{2}')".format(nom.get(), aps.get(), certificate_bytes)
    mycursor.execute(sql)
    db.commit()
    db.close()

def salir(window):
    window.withdraw()

def on_closing():
    creation_win.destroy()

def inicializar_programa():
    global creation_win
    
    db = mysql.connector.connect(

    host="localhost",
    user="root",
    passwd="root",
    database="testdatabase"
    )
    mycursor = db.cursor()
    
    try:
        mycursor.execute("CREATE TABLE Usuario (nombre VARCHAR(50), apellido VARCHAR(50), username VARCHAR(50), password VARCHAR(250), estado VARCHAR(50), localidad VARCHAR(50), rol VARCHAR(10), id int PRIMARY KEY AUTO_INCREMENT)")
        mycursor.execute("CREATE TABLE Authutils (nombre VARCHAR(50), apellido VARCHAR(50), certificate VARCHAR(3000), id int PRIMARY KEY AUTO_INCREMENT)")
        
        messagebox.showinfo(message="Se ha creado con éxito la base de datos", title="Base de datos creada")
        
        creation_win = Tk()
        creation_win.geometry("600x200")
        creation_win.title("Optima App")
        creation_win.configure(bg = "#C2E5E5")
        creation_win.iconbitmap(r"C:\Users\carav\OneDrive\Escritorio\Optima App\optima.ico")

        global image 
        image = PhotoImage(file=r"C:\Users\carav\OneDrive\Escritorio\Optima App\blackopt.png")
        Label(creation_win, text= "Bienvenido, al hacer clic se iniciará la creación \n del primer administrador", bg= "#C2E5E5", font= ("Pointer One", 16)).pack(pady=5)
        Button(creation_win, text= "Inicializar programa", anchor="center", relief="raised", borderwidth=3, command= lambda: [create_user()], background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=5)
        creation_win.protocol("WM_DELETE_WINDOW", on_closing)
        mainloop()
    
    except:
        messagebox.showinfo(message="Se procederá a la ventana de creación de administrador.",
                            title="Base ya existente")
        creation_win = Tk()
        creation_win.geometry("600x200")
        creation_win.title("Optima App")
        creation_win.configure(bg = "#C2E5E5")
        creation_win.iconbitmap(r"C:\Users\carav\OneDrive\Escritorio\Optima App\optima.ico")

        image = PhotoImage(file=r"C:\Users\carav\OneDrive\Escritorio\Optima App\blackopt.png")
        Label(creation_win, text= "Bienvenido, al hacer clic se iniciará la creación \n del primer administrador", bg= "#C2E5E5", font= ("Pointer One", 16)).pack(pady=5)
        Button(creation_win, text= "Inicializar programa", anchor="center", relief="raised", borderwidth=3, command= lambda: [create_user()], background= "#E3B01E", font= ("Pointer One", 12)).pack(pady=5)
        creation_win.protocol("WM_DELETE_WINDOW", on_closing)
        mainloop()